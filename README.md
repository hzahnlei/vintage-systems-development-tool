# Vintage Systems Development Tool

Hardware and software for testing vintage integrated circuits. Based on Arduino Micro (ATmega32U4).

## Functional principle
The device contains an Arduino Micro with a ATmega32U4 MCU called the HOST CPU.
The HOST can read and write to an so called SHARED MEMORY SPACE.
Vintage ICs like memory or I/O chips can be added to this SHARED MEMORY by means of a
connector.
However, there is a second connector.
A (vintage) GUEST CPU an be pugged into this connector.
The HOST can give control over the SHARED MEMORY to the GUEST.

Doing so, one can conveniently copy assembly programs from a computer into the SHARED
memory and then have a vintage CPU execute it.
Hence, no EPROM programmer is required.
Also, one can halt the GUEST and inspect and manipulate the SHARED MEMORY.
By the way, registers of I/O chips are also just memory.

## Usage
1. Plug in the power plug.
2. Connect the device to your computer via USB cable.
3. Open your favorite terminal application.
4. Make sure your terminal is configured like so:
   - 9.600 8N1
   - Raw
   - Interpret BELL, BS and DEL
5. Connect your terminal to the USB serial port opened for the device.
6. Type commands as per your need.

### Convenience functions
The firmware does not implement a fully fledged line editor.
However, some convenience functions are there at your disposal.

| Key         | Explanation                                           |
| ----------- | ----------------------------------------------------- |
| Backspace   | Delete the last character you typed.                  |
| Cursor up   | Scroll backward in the history of commands you typed. |
| Cursor down | Scroll forward in your command history.               |

Only valid commands that have been executed successfully will end up in the history.
Use backspace to modify the selected command.
Press enter to execute the selected command.

### Commands
| Command | Parameters                  | Explanation                                      |
| ------- | --------------------------- | ------------------------------------------------ |
| ?       | n/a                         | **Help**: Displays a simple help text.           |
| ?       | _addr_                      | **Peek**: Displays the value stored in memory cell _addr_. |
| ?       | _from_addr_ _to_addr_       | **Dump**: Displays all values stored in consecutive memorycells beginning at _from_addr_ ending at _to_addr_. |
| !       | _addr_ _val_                | **Poke**: Stores value _val_ at memory location _addr_.  |
| !       | _addr_ _val1_ _val2_ ...    | **Poke**: Stores values _val1_, _val2_ etc beginning at memory location _addr_.  |
| :       | _intel hex_                 | **Poke**: Processes the given Intel hex record. Supported record types are 0 (data) and 1 (end). Can be used to copy/paste assembler output (Intel hex) into the memory via terminal.  |
| *       | _from_addr_ _to_addr_ _val_ | **Fill**: Stores the value _val_ in all memory locations beginning at _from_addr_ ending at _to_addr_. |
| =       | _from_addr_ _to_addr_       | **Verfify**: Verifies that all memory locations beginning at _from_addr_ ending at _to_addr_ are functioning correctly. **Be advised, this command is destructive.** It writes to memory multiple times, altering the former content. |
| status  |                             | **Status**: Displays whether guest is running and busmaster or guest is halted and host is busmaster. |
| halt    |                             | **Halt**: Halts the guest CPU. Host becomes bus master. |
| run     |                             | **Run**: Guest becomes bus master and executes programs. |
| reset   |                             | **Reset**: A short pulse (falling edge) on the hardware reset line. |
| int     |                             | **Interrupt**: A short pulse (falling edge) on the hardware interrupt line. Can be used to trigger interrupt service routines (ISR) on the guest CPU. |

Examples:
- This `? 0 1ff` prints the content of memory locations from 0x0000 to 0x01ff.
- This `! c000 a0 a1 a2 a3 a4` sets memory location 0xc000 to 0xa0, memory location 0xc001 to
0xa1, memory location 0xc002 to 0xa2 etc.

# 6502 Example
By typing
```
halt
! 1000 78 D8 A9 FF 8D 81 40 A9 00 8D 80 40 58 4C 0D 10 E3
! 1010 40 49 FF 8D 80 40 40 C4
! FFFA 10 10 00 10 11 10
reset
run
int
````
One places a 6502 program into the shared memory at 0x1000.
The hardware vectores are placed from 0xfffa to 0xffff.
By typing `halt` we halt the guest CPU and become busmaster.
With the `!` command program and data will be placed on the sherd memory at the required locations.
`reset`ing the guest is a good idea to make it start correctly.
After typing `run` the guest CPU executes its program.
(Actually this main program does nothig but busy-looping.)
By typing `int` an ISR will be triggerd on the guest CPU.
(The port A of an 6532 RIOT at 0x4080 will be toggled.
This can be made visible very effectfully by an LED attached to one of port A's pins.)