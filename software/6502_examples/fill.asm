;==============================================================================
; Fills 256 bytes of memory beginning at 0x2000 with a value of 0x55.
; Consequently, the program expects RAM at 0x2000 to 0x20ff.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf fill.hex, int

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x0000
MAIN:
		sei				; Disable interrupts
		cld				; Use binary instead of BCD arithmetic
		lda #0x55
		ldx #0x00
.LOOP:
		sta 0x2000, x
		inx
		jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
		rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR		; NMI vector
		.dw MAIN		; RESET vector
		.dw IRQ_BRK_ISR	; IRQ and BRK vector

		.end
