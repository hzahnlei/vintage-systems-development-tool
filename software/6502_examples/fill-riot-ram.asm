;==============================================================================
; Fills 256 bytes of RIOT memory beginning at 0x4000 with a value of 0x55.
; Consequently, the program expects the RIOT chip RAM at 0x4000.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf fill-riot-ram.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------
RIOT_RAM  = 0x4000

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x0000
MAIN:
		sei				; Disable interrupts
		cld				; Use binary instead of BCD arithmetic
		lda #0x55
		ldx #0x00
.LOOP:
		sta RIOT_RAM, x
		inx
		jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
		rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR		; NMI vector
		.dw MAIN		; RESET vector
		.dw IRQ_BRK_ISR	; IRQ and BRK vector

		.end
