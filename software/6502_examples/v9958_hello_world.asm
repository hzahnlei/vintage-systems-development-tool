;==============================================================================
; Initializes some palette registers of the V9958 and then, in a loop, alters
; the backdrop color by cycling palette entries. This program requires a
; minimal wiring and no video RAM. It is meant for testing whether a V9958 is
; working or not.
;
; The program expects the V9958's 4 adress locations to be mapped starting at
; 0x8000.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
; 6502 runs at 2MHz.
; The screen (character map) is located at 0x0000 in the VDP's own video RAM.
; The character patterns are located starting at 0x0800 in the VDP's own video
; RAM. 0x0800-0x0807 are all zeros to represent the SPACE character.
;
; This is an 6502 port from Timo "NYYRIKKI" Soilamaa Z80 version:
; http://helloworldcollection.de/#Assembler%C2%A0(Z80%C2%A0Console)
;
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf v9958_hello_world.hex, int

V9958_DATA_REG_ADDR = 0x8000
V9958_CTL_REG_ADDR = 0x8001
V9958_CTL_REG = 0b10000000
; No blanks allowed around +, but no error message. Fails silently!
V9958_CTL_REG_00 = 0x00+V9958_CTL_REG	; Modes and horizontal interrupts
V9958_CTL_REG_01 = 0x01+V9958_CTL_REG	; Modes and vertical interrupts
V9958_CTL_REG_02 = 0x02+V9958_CTL_REG	; Character map A10-A16
V9958_CTL_REG_04 = 0x04+V9958_CTL_REG	; Character patterns A11-A16
V9958_CTL_REG_07 = 0x07+V9958_CTL_REG	; Text and backdrop color

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x0000
MAIN:
		sei				; Disable interrupts
		cld				; Use binary instead of BCD arithmetic

        ; Set VDP write address to 0x0000, ignore register #14 (A14-A16)
        lda #0x00       ; A0-A7
        sta V9958_CTL_REG_ADDR
        lda #0x40       ; A8-A13 + W(rite)
        sta V9958_CTL_REG_ADDR

        ; Clear first 16Kb of VDP memory
        lda #0x00
        ldx #0x3f       ; Just need to count the bytes deletd
        ldy #0xff       ; The address is autoincremented by VDP
.CLEAR:
        sta V9958_DATA_REG_ADDR
        nop             ; Give VDP time, TODO: see if this actually necessary
        nop
        nop
        nop
        dey
        bne .CLEAR
        dex
        bne .CLEAR

        ; Set up VDP registers

        ; Register #0 = 0x00
        ; Clear mode selection bits M3, M4 and M5
        ; Disable external video and horizontal interrupt
        lda #0x00
        sta V9958_CTL_REG_ADDR
        lda #V9958_CTL_REG_00
        sta V9958_CTL_REG_ADDR

        ; Register #1 = 0x50
        ; 40 column mode, clear mode selection bits M1, M2
        ; Disable vertical interrupt
        lda #0x50
        sta V9958_CTL_REG_ADDR
        lda #V9958_CTL_REG_01
        sta V9958_CTL_REG_ADDR

        ; Register #2 = 0x00
        ; Set character map address to #0000
        lda #0x00
        sta V9958_CTL_REG_ADDR
        lda #V9958_CTL_REG_02
        sta V9958_CTL_REG_ADDR

        ; Register #3 is ignored as 40 column mode does not need color table

        ; Register #4 = 0x01
        ; Set character pattern address to 0x0800
        lda #0x01
        sta V9958_CTL_REG_ADDR
        lda #V9958_CTL_REG_04
        sta V9958_CTL_REG_ADDR

        ; Registers #5 (Sprite attribute) and #6 (Sprite pattern) are ignored
        ; as 40 column mode does not have sprites

        ; Register #7 = 0xf0
        ; Set colors to white on black
        lda #0xf0
        sta V9958_CTL_REG_ADDR
        lda #V9958_CTL_REG_07
        sta V9958_CTL_REG_ADDR

        ; Set VDP write address to 0x0808, ignore register #14 (A14-A16)
        ; Character data will reside here, 0x0800-0x807 is SPACE (clear)
        lda #0x08       ; A0-A7
        sta V9958_CTL_REG_ADDR
        lda #0x48       ; A8-A13 + W(rite)
        sta V9958_CTL_REG_ADDR

        ; Copy character set into VDP memory
        ldx #0x00
.COPY_CHARS:
        lda CHAR_DATA_BEGIN, x
        sta V9958_DATA_REG_ADDR
        inx
        nop
        nop
        nop
        nop
        cpx #NB_OF_CHAR_BYTES
        bne .COPY_CHARS

        ; Set VDP write address to 0x0000, ignore register #14 (A14-A16)
        ; Character map will reside here
        lda #0x00       ; A0-A7
        sta V9958_CTL_REG_ADDR
        lda #0x40       ; A8-A13 + W(rite)
        sta V9958_CTL_REG_ADDR

        ; Write "Hello World" on screen
        ldx #0x00
.COPY_HELLO_TEXT:
        lda HELLO_TEXT_BEGIN, x
        sta V9958_DATA_REG_ADDR
        inx
        nop
        nop
        nop
        nop
        cpx #NB_OF_HELLO_CHARS
        bne .COPY_HELLO_TEXT

.LOOP:
		jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
		rti

;------------------------------------------------------------------------------
; Data
;------------------------------------------------------------------------------
HELLO_TEXT_BEGIN:
        .db 1,2,3,3,4,0,5,4,6,3,7
HELLO_TEXT_END:
NB_OF_HELLO_CHARS = HELLO_TEXT_END-HELLO_TEXT_BEGIN

CHAR_DATA_BEGIN:
        ; H
        .db %10001000
        .db %10001000
        .db %10001000
        .db %11111000
        .db %10001000
        .db %10001000
        .db %10001000
        .db %00000000
        ; e
        .db %00000000
        .db %00000000
        .db %01110000
        .db %10001000
        .db %11111000
        .db %10000000
        .db %01110000
        .db %00000000
        ; l
        .db %01100000
        .db %00100000
        .db %00100000
        .db %00100000
        .db %00100000
        .db %00100000
        .db %01110000
        .db %00000000
        ; o
        .db %00000000
        .db %00000000
        .db %01110000
        .db %10001000
        .db %10001000
        .db %10001000
        .db %01110000
        .db %00000000
        ; W
        .db %10001000
        .db %10001000
        .db %10001000
        .db %10101000
        .db %10101000
        .db %11011000
        .db %10001000
        .db %00000000
        ; r
        .db %00000000
        .db %00000000
        .db %10110000
        .db %11001000
        .db %10000000
        .db %10000000
        .db %10000000
        .db %00000000
        ; d
        .db %00001000
        .db %00001000
        .db %01101000
        .db %10011000
        .db %10001000
        .db %10011000
        .db %01101000
        .db %00000000
CHAR_DATA_END:
NB_OF_CHAR_BYTES = CHAR_DATA_END-CHAR_DATA_BEGIN

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR		; NMI vector
		.dw MAIN		; RESET vector
		.dw IRQ_BRK_ISR	; IRQ and BRK vector

		.end
