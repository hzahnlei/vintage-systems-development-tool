###############################################################################
# Building several example (cross) assembly programs for vintage MPUs.
#
# Uses SBASM cross assembler. Expects the SBASM executable to be in the PATH.
# SBASM is written in Python. So, Python needs to be installed too.
#
# (c) 2019 by Holger Zahnleiter, all rights reserved
###############################################################################

.PHONY: clean

clean:
	rm -f *.hex

all: clean fill.hex fill-riot-ram.hex riot-blink.hex irq-test.hex riot-irq-blink.hex riot-irq-pwm.hex acia-ok.hex ym2612.hex read_v9958_stat_reg.hex v9958_bg_color_only.hex v9958_hello_world.hex

fill.hex: fill.asm
	sbasm fill

fill-riot-ram.hex: fill-riot-ram.asm
	sbasm fill-riot-ram

riot-blink.hex: riot-blink.asm
	sbasm riot-blink

irq-test.hex: irq-test.asm
	sbasm irq-test

riot-irq-blink.hex: riot-irq-blink.asm
	sbasm riot-irq-blink

riot-irq-pwm.hex: riot-irq-pwm.asm
	sbasm riot-irq-pwm

acia-ok.hex: acia-ok.asm
	sbasm acia-ok

ym2612.hex: ym2612.asm
	sbasm ym2612

read_v9958_stat_reg.hex: read_v9958_stat_reg.asm
	sbasm read_v9958_stat_reg

v9958_bg_color_only.hex: v9958_bg_color_only.asm
	sbasm v9958_bg_color_only

v9958_hello_world.hex: v9958_hello_world.asm
	sbasm v9958_hello_world

ppi8255test.hex: ppi8255test.asm
	sbasm ppi8255test

idt7132test.hex: idt7132test.asm
	sbasm idt7132test
