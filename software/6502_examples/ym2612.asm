;==============================================================================
;
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
	.cr 6502
	.tf ym2612.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------
YM_ADDR_REG_ADDR	= 0x6000
YM_DATA_REG_ADDR	= 0x6001
YM_REG_22			= 0x22
YM_REG_27			= 0x27
YM_REG_28			= 0x28
YM_REG_2B			= 0x2b
YM_REG_30			= 0x30
YM_REG_34			= 0x34
YM_REG_38			= 0x38
YM_REG_3C			= 0x3c
YM_REG_40			= 0x40
YM_REG_44			= 0x44
YM_REG_48			= 0x48
YM_REG_4C			= 0x4c
YM_REG_50			= 0x50
YM_REG_54			= 0x54
YM_REG_58			= 0x58
YM_REG_5C			= 0x5c
YM_REG_60			= 0x60
YM_REG_64			= 0x64
YM_REG_68			= 0x68
YM_REG_6C			= 0x6c
YM_REG_70			= 0x70
YM_REG_74			= 0x74
YM_REG_78			= 0x78
YM_REG_7C			= 0x7c
YM_REG_80			= 0x80
YM_REG_84			= 0x84
YM_REG_88			= 0x88
YM_REG_8C			= 0x8c
YM_REG_90			= 0x90
YM_REG_94			= 0x94
YM_REG_98			= 0x98
YM_REG_9C			= 0x9c
YM_REG_A0			= 0xa0
YM_REG_A4			= 0xa4
YM_REG_B0			= 0xb0
YM_REG_B4			= 0xb4

TXDATA		= 0x4000
RXDATA		= 0x4000
STATUSREG	= 0x4001
CMDREG		= 0x4002
CTLREG		= 0x4003
CR			= 0x0A

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
	.org 0x1000
MAIN:
	sei					; Disable interrupts
	cld					; Use binary instead of BCD arithmetic

    lda #0x1e			; 8N1, internal clock, 9,600bps
    sta CTLREG
    lda #0x0b			; No parity, no echo, no TX interrupts, RTS low, no RX interrupts, DTR low
    sta CMDREG

    ldx #YM_REG_22	; LFO off
    ldx #0x00
    jsr SEND_YM

    ldx #YM_REG_27	; Note off CH0
    ldy #0x00
    jsr SEND_YM
    ldx #YM_REG_28	; Note off CH1
    ldy #0x01
    jsr SEND_YM
    ldx #YM_REG_28	; Note off CH2
    ldy #0x02
    jsr SEND_YM
    ldx #YM_REG_28	; Note off CH3
    ldy #0x04
    jsr SEND_YM
    ldx #YM_REG_28	; Note off CH4
    ldy #0x05
    jsr SEND_YM
    ldx #YM_REG_28	; Note off CH5
    ldy #0x06
    jsr SEND_YM

    ldx #YM_REG_2B	; DAC off
    ldy #0x00

    jsr SEND_YM		; DT1/MUL
	ldx #YM_REG_30
    ldy #0x71
    jsr SEND_YM
	ldx #YM_REG_34
    ldy #0x0d
    jsr SEND_YM
	ldx #YM_REG_38
    ldy #0x33
    jsr SEND_YM
	ldx #YM_REG_3C
    ldy #0x01
    jsr SEND_YM

	ldx #YM_REG_40	; Total level
    ldy #0x23
    jsr SEND_YM
	ldx #YM_REG_44
    ldy #0x2d
    jsr SEND_YM
	ldx #YM_REG_48
    ldy #0x26
    jsr SEND_YM
	ldx #YM_REG_4C
    ldy #0x00
    jsr SEND_YM

	ldx #YM_REG_50	; RS/AR
    ldy #0x5f
    jsr SEND_YM
	ldx #YM_REG_54
    ldy #0x99
    jsr SEND_YM
	ldx #YM_REG_58
    ldy #0x5f
    jsr SEND_YM
	ldx #YM_REG_5C
    ldy #0x94
    jsr SEND_YM

	ldx #YM_REG_60	; AM/D1R
    ldy #0x05
    jsr SEND_YM
	ldx #YM_REG_64
    ldy #0x05
    jsr SEND_YM
	ldx #YM_REG_68
    ldy #0x05
    jsr SEND_YM
	ldx #YM_REG_6C
    ldy #0x07
    jsr SEND_YM

	ldx #YM_REG_70	; D2R
    ldy #0x02
    jsr SEND_YM
	ldx #YM_REG_74
    ldy #0x02
    jsr SEND_YM
	ldx #YM_REG_78
    ldy #0x02
    jsr SEND_YM
	ldx #YM_REG_7C
    ldy #0x02
    jsr SEND_YM

	ldx #YM_REG_80	; D1L/RR
    ldy #0x11
    jsr SEND_YM
	ldx #YM_REG_84
    ldy #0x11
    jsr SEND_YM
	ldx #YM_REG_88
    ldy #0x11
    jsr SEND_YM
	ldx #YM_REG_8C
    ldy #0xa6
    jsr SEND_YM

	ldx #YM_REG_90	; Proprietary
    ldy #0x00
    jsr SEND_YM
	ldx #YM_REG_94
    ldy #0x00
    jsr SEND_YM
	ldx #YM_REG_98
    ldy #0x00
	jsr SEND_YM
	ldx #YM_REG_9C
    ldy #0x00
    jsr SEND_YM

	ldx #YM_REG_B0	; Feedback/algorithm
    ldy #0x32
    jsr SEND_YM

	ldx #YM_REG_B4	; Both speakers on
    ldy #0xc0
    jsr SEND_YM

	ldx #YM_REG_28	; Key off
    ldy #0x00
    jsr SEND_YM

	ldx #YM_REG_A4	; Frequency
    ldy #0x22
    jsr SEND_YM
	ldx #YM_REG_A0
    ldy #0x69
    jsr SEND_YM

.LOOP:
    jsr PRINT_ON
    ldx #YM_REG_28 ; Key on
    ldy #0xf0
    jsr SEND_YM
    jsr DELAY
    jsr PRINT_OFF
    ldx #YM_REG_28 ; Key off
    ldy #0x00
    jsr SEND_YM
    jsr DELAY
    jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------
SEND_YM:
    stx YM_ADDR_REG_ADDR
    sty YM_DATA_REG_ADDR
    rts

PRINT_ON:
    lda #'O'
    jsr PUT_CHAR
    lda #'N'
    jsr PUT_CHAR
    lda #CR
    jsr PUT_CHAR
    rts

PRINT_OFF:
    lda #'O'
    jsr PUT_CHAR
    lda #'F'
    jsr PUT_CHAR

    lda #'F'
    jsr PUT_CHAR
    lda #CR
    jsr PUT_CHAR
    rts

PUT_CHAR:
    pha
    lda #0x10
.TXFULL:
    bit STATUSREG   ; wait for TDRE bit = 1
    beq .TXFULL
    pla
    sta TXDATA
    rts

DELAY:
	lda #0x0a
	sta 0
    ldx #0xff
    ldy #0xff
.LOOP
    dex
    bne .LOOP
    dey
    bne .LOOP
	dec 0
	bne .LOOP
    rts

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
	rti

IRQ_BRK_ISR:
    rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
	.org 0xfffa
	.dw NMI_ISR		; NMI vector
	.dw MAIN		; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
