;==============================================================================
; Fills 2k bytes of memory beginning at 0x4000 with a value of 0xaa.
; Consequently, the program expects RAM at 0x4000 to 0x47ff.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
;
; Any (static) RAM could reside at 0x4000 - 0x47ff.
; However, I was writing this program to test an IDT 7132 dual port RAM.
; Therefore the name of the program.
; The IDT 7132 has only 2kB of RAM
;
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2023 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf idt7132test.hex, int

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x0000

DEST_ADDR:
		.dw 0x4000              ; Store address pointer here

MAIN:
		sei                     ; Disable interrupts
		cld                     ; Use binary instead of BCD arithmetic

		lda #0x00               ; Set address pointer -> 0x4000
		sta DEST_ADDR
		lda #0x40
		sta DEST_ADDR+1

		lda #0xaa               ; Value to be set
		ldy #0x00               ; Byte index within page
		ldx #0x08               ; Pages (á 256 bytes) to be overwritten
.LOOP:
		sta (DEST_ADDR),y
		iny
		bne .LOOP
		inc DEST_ADDR+1
		dex
		bne .LOOP

.HALT_HERE	jmp .HALT_HERE

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
		rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR		; NMI vector
		.dw MAIN		; RESET vector
		.dw IRQ_BRK_ISR         ; IRQ and BRK vector

		.end
