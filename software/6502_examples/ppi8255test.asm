;==============================================================================
; Testing 8255 chips. Therefore defines all ports to be outputs in mode 0. Then
; sets all pins high. The program expects the 8255 chip to live in 8k page two,
; at 0x4000.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2023 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf ppi8255test.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------
PPI_ADDR    = 0x4000
PPI_PORT_A  = PPI_ADDR+0
PPI_PORT_B  = PPI_ADDR+1
PPI_PORT_C  = PPI_ADDR+2
PPI_CONTROL = PPI_ADDR+3

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x0000
MAIN:
		sei             ; Disable interrupts
		cld             ; Use binary instead of BCD arithmetic
		lda #0b10000000 ; Make all ports outputs in mode 0
		sta PPI_CONTROL
		lda #0xff       ; Set all pins of all ports high
		sta PPI_PORT_A
		sta PPI_PORT_B
		sta PPI_PORT_C
.LOOP:
		jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
		rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR     ; NMI vector
		.dw MAIN        ; RESET vector
		.dw IRQ_BRK_ISR ; IRQ and BRK vector

		.end
