;==============================================================================
; Initializes some palette registers of the V9958 and then, in a loop, alters
; the backdrop color by cycling palette entries. This program requires a
; minimal wiring and no video RAM. It is meant for testing whether a V9958 is
; working or not.
; The program expects the V9958's 4 adress locations to be mapped starting at
; 0x8000.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf v9958_bg_color_only.hex, int

V9958_CTL_REG_ADDR = 0x8001
V9958_CTL_REG = 0b10000000
; No blanks allowed around +, but no error message. Fails silently!
V9958_CTL_REG_07 = 0x07+V9958_CTL_REG	; Text and backdrop color
V9958_CTL_REG_16 = 0x10+V9958_CTL_REG	; Palette entry index
V9958_PALETTE_REG_ADDR = 0x8002
V9958_PALETTE_REG_00 = 0x00

COLOR_BLUE_LOW 		= 0b00000010	; R, B
COLOR_BLUE_HIGH		= 0b00000000	; G

COLOR_RED_LOW 		= 0b00110000	; R, B
COLOR_RED_HIGH		= 0b00000000	; G

COLOR_GREEN_LOW 	= 0b00000000	; R, B
COLOR_GREEN_HIGH	= 0b00000010	; G

COLOR_WHITE_LOW 	= 0b01110111	; R, B
COLOR_WHITE_HIGH	= 0b00000111	; G

COLOR_PURPLE_LOW 	= 0b01000100	; R, B
COLOR_PURPLE_HIGH	= 0b00000000	; G

COLOR_YELLOW_LOW 	= 0b01110000	; R, B
COLOR_YELLOW_HIGH	= 0b00000111	; G

COLOR_ORANGE_LOW 	= 0b01100000	; R, B
COLOR_ORANGE_HIGH	= 0b00000100	; G

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x0000
MAIN:
		sei				; Disable interrupts
		cld				; Use binary instead of BCD arithmetic

		jsr SET_COLOR_PALETTE

.LOOP:
		; Use palette entry/color 0 as backdrop color.
		lda #0
		jsr SET_BACKDROP_COLOR
		jsr DELAY

		; Use palette entry/color 1 as backdrop color.
		lda #1
		jsr SET_BACKDROP_COLOR
		jsr DELAY

		; Use palette entry/color 2 as backdrop color.
		lda #2
		jsr SET_BACKDROP_COLOR
		jsr DELAY

		; Use palette entry/color 3 as backdrop color.
		lda #3
		jsr SET_BACKDROP_COLOR
		jsr DELAY

		; Use palette entry/color 4 as backdrop color.
		lda #4
		jsr SET_BACKDROP_COLOR
		jsr DELAY

		; Use palette entry/color 5 as backdrop color.
		lda #5
		jsr SET_BACKDROP_COLOR
		jsr DELAY

		; Use palette entry/color 6 as backdrop color.
		lda #6
		jsr SET_BACKDROP_COLOR
		jsr DELAY

		jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------
SET_COLOR_PALETTE:
		; Start with palette entry 0. Reg 16 will be auto-incremented such that
		; all palette entries can be set subsequently without setting control
		; register 16 all the time.
		lda #V9958_PALETTE_REG_00
		ldx #V9958_CTL_REG_16
		jsr SET_9958_CTL_REG
		; Set palette entry 0.
		lda #COLOR_BLUE_LOW			; R + B
		sta V9958_PALETTE_REG_ADDR
		lda #COLOR_BLUE_LOW			; G
		sta V9958_PALETTE_REG_ADDR
		; Set palette entry 1.
		lda #COLOR_RED_LOW			; R + B
		sta V9958_PALETTE_REG_ADDR
		lda #COLOR_RED_HIGH			; G
		sta V9958_PALETTE_REG_ADDR
		; Set palette entry 2.
		lda #COLOR_GREEN_LOW		; R + B
		sta V9958_PALETTE_REG_ADDR
		lda #COLOR_GREEN_HIGH		; G
		sta V9958_PALETTE_REG_ADDR
		; Set palette entry 3.
		lda #COLOR_WHITE_LOW		; R + B
		sta V9958_PALETTE_REG_ADDR
		lda #COLOR_WHITE_HIGH		; G
		sta V9958_PALETTE_REG_ADDR
		; Set palette entry 4.
		lda #COLOR_PURPLE_LOW		; R + B
		sta V9958_PALETTE_REG_ADDR
		lda #COLOR_PURPLE_HIGH		; G
		sta V9958_PALETTE_REG_ADDR
		; Set palette entry 5.
		lda #COLOR_YELLOW_LOW		; R + B
		sta V9958_PALETTE_REG_ADDR
		lda #COLOR_YELLOW_HIGH		; G
		sta V9958_PALETTE_REG_ADDR
		; Set palette entry 6.
		lda #COLOR_ORANGE_LOW		; R + B
		sta V9958_PALETTE_REG_ADDR
		lda #COLOR_ORANGE_HIGH		; G
		sta V9958_PALETTE_REG_ADDR

		rts

SET_BACKDROP_COLOR:
		; A = Palette entry (0-15)
		sta V9958_CTL_REG_ADDR
		lda #V9958_CTL_REG_07
		sta V9958_CTL_REG_ADDR

		rts

SET_9958_CTL_REG:
		; A = value
		; X = V9958 control register number
		; Put value to control register port.
		sta V9958_CTL_REG_ADDR
		; Put register number to control control port, and ultimately put
		; value to control register with this second write.
		stx V9958_CTL_REG_ADDR

		rts

DELAY:
		lda #0x10
        ldx #0xff
        ldy #0xff
.LOOP:
        dex
        bne .LOOP
        dey
        bne .LOOP
        clc
		sbc #0x01
		cmp #0x00
		bne .loop

		rts

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
		rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR		; NMI vector
		.dw MAIN		; RESET vector
		.dw IRQ_BRK_ISR	; IRQ and BRK vector

		.end
