;==============================================================================
; Repeatedly sends "OK\n". Configures the 6551 ACIA to 8N1@9,600 Baud.
; Assumes a 1.8432MHz clock. ACIA at 0x4000, program at 0x1000 and hardware
; vectors from 0xfffa to 0xffff. Does not use interrupts.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
	    .cr 6502
	    .tf acia-ok.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------
TXDATA      = 0x4000
RXDATA      = 0x4000
STATUSREG   = 0x4001
CMDREG      = 0x4002
CTLREG      = 0x4003
CR          = 0x0A

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
        .org 0x1000
MAIN:
	    sei		    ; Disable interrupts
	    cld		    ; Use binary instead of BCD arithmetic
        lda #0x1e   ; 8N1, internal clock, 9,600bps
        sta CTLREG
        lda #0x0b   ; No parity, no echo, no TX interrupts, RTS low, no RX interrupts, DTR low
        sta CMDREG
.LOOP:
        lda #'O'
        jsr PUT_CHAR
        jsr DELAY
        lda #'K'
        jsr PUT_CHAR
        jsr DELAY
        lda #CR
        jsr PUT_CHAR
        jsr DELAY
        jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------
PUT_CHAR:
        pha
        lda #0x10
.TXFULL:
        bit STATUSREG   ; wait for TDRE bit = 1
        beq .TXFULL
        pla
        sta TXDATA
        rts

DELAY:
        ldx #0xff
        ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
	    rti

IRQ_BRK_ISR:
	    rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
        .org 0xfffa
        .dw NMI_ISR     ; NMI vector
        .dw MAIN        ; RESET vector
        .dw IRQ_BRK_ISR ; IRQ and BRK vector

        .end
