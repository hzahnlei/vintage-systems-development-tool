;==============================================================================
; Reads status registers #1 and #2 from the V9958 VDP and stores it at 0x1000
; and 0x1001.
; Consequently, the program expects RAM at 0x1000 to 0x1001.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf read_v9958_stat_reg.hex, int

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x0000
MAIN:
		sei				; Disable interrupts
		cld				; Use binary instead of BCD arithmetic

		lda #0x01
		sta $8001
		nop
		nop
		nop
		nop
		lda #0x8f
		sta $8001
		nop
		nop
		nop
		nop
		lda $8001
		sta $1000

		nop
		nop
		nop
		nop

		lda #0x02
		sta $8001
		nop
		nop
		nop
		nop
		lda #0x8f
		sta $8001
		nop
		nop
		nop
		nop
		lda $8001
		sta $1001
.LOOP:
		jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
		rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR		; NMI vector
		.dw MAIN		; RESET vector
		.dw IRQ_BRK_ISR	; IRQ and BRK vector

		.end
