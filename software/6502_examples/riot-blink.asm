;==============================================================================
; Controls a 6532 RIOT to let an LED blink. Only uses the RIOT's I/O port. No
; timer. No interrupts. Delay is achived by busy-waiting.
; Assumes RAM for program at 0x0000 and hardware vectors from 0xfffa to 0xffff.
; RIOT RAM is located at 0x4000 (not used) and RIOT registers are located at
; 0x4080.
; Blinking is relatively high frequent when running a 65C02@4MHz.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf riot-blink.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------
RIOT_PORTA  = 0x4080
RIOT_DDRA   = 0x4081

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
        .org 0x0000
MAIN:
        sei		; Disable interrupts
        cld		; Use binary instead of BCD arithmetic
        lda #0xff       ; All pins are outputs
        sta RIOT_DDRA
.LOOP:
        lda #0x00
        sta RIOT_PORTA  ; LED off
        jsr DELAY
        lda #0xff
        sta RIOT_PORTA  ; LED on
        jsr DELAY
        jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------
DELAY:
        ldx #0xff
        ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
	    rti

IRQ_BRK_ISR:
	    rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
        .org 0xfffa
        .dw NMI_ISR     ; NMI vector
        .dw MAIN        ; RESET vector
        .dw IRQ_BRK_ISR ; IRQ and BRK vector

        .end
