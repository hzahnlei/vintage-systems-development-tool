;==============================================================================
; Uses an 6532 RIOT timer to toggle port A.
; Program at 0x1000, RIOT RAM at 0x4000, RIOT registers at 0x4080 and hardware
; vectors at 0xfffa.
; SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
;
; (c) 2019 by Holger Zahnleiter, all rights reserved
;==============================================================================
		.cr 6502
		.tf riot-irq-blink.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------
RIOT_PORTA          = 0x4080
RIOT_DDRA           = 0x4081
RIOR_TIMER_1024_IRQ = 0x409f

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------
		.org 0x1000
MAIN:
		sei				; Disable interrupts
		cld				; Use binary instead of BCD arithmetic
		lda #0xff       ; All pins are outputs
		sta RIOT_DDRA
        lda #0x00
        sta RIOT_PORTA
        lda #0xff
        sta RIOR_TIMER_1024_IRQ
        cli             ; Enable interrupts
.LOOP:
        jmp .LOOP

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------
NMI_ISR:
		rti

IRQ_BRK_ISR:
        lda RIOT_PORTA
        eor #0xff
        sta RIOT_PORTA
        lda #0xff
        sta RIOR_TIMER_1024_IRQ
        rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		.org 0xfffa
		.dw NMI_ISR		; NMI vector
		.dw MAIN		; RESET vector
		.dw IRQ_BRK_ISR	; IRQ and BRK vector

		.end
