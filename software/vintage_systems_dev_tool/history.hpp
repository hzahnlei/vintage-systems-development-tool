/*!
   (c) 2019 Holger Zahnleiter. All righs reserved.
*/

#ifndef VINTAGE_SYSTEMS_DEV__HISTORY_HPP_
#define VINTAGE_SYSTEMS_DEV__HISTORY_HPP_

namespace vsdt
{

constexpr uint8_t HISTORY_SIZE = 8;

/*!
   /brief A command history for the use's convenience.
*/
class command_history final
{
  public:
    auto remember(const String &command) -> void
    {
      if (needs_to_be_remembered(command))
      {
        entry[head] = command;
        head++;
        head %= HISTORY_SIZE;
        if (count < HISTORY_SIZE)
        {
          count++;
        }
      }
      scrolling_count = count;
      scrolling_idx = head;
    }

    static const String NO_ENTRY;

    [[nodiscard]] auto previous() const -> const String &
    {
      if (is_empty() || is_at_top())
      {
        return NO_ENTRY;
      }
      scrolling_count--;
      if (0 == scrolling_idx)
      {
        scrolling_idx = count;
      }
      scrolling_idx--;
      return entry[scrolling_idx];
    }

    [[nodiscard]] auto next() const -> const String &
    {
      if (is_empty() || is_at_bottom())
      {
        return NO_ENTRY;
      }
      scrolling_count++;
      scrolling_idx++;
      scrolling_idx %= count;
      return entry[scrolling_idx];
    }

  private:
    [[nodiscard]] inline auto is_empty(const String& command) const -> bool
    {
      return command.equals("");
    }
    
    [[nodiscard]] inline auto is_intel_hex_record(const String& command) const -> bool
    {
      return command.startsWith(":");
    }

    [[nodiscard]] inline auto is_help(const String& command) const -> bool
    {
      return command.equals("?");
    }
    
    [[nodiscard]] inline auto needs_to_be_remembered(const String& command) const -> bool
    {
      return !is_empty(command) && !is_help(command) && !is_intel_hex_record(command) && !already_stored(command);
    }

    [[nodiscard]] auto already_stored(const String& command) const -> bool
    {
      for (auto i = 0U; i < HISTORY_SIZE; i++)
      {
        if (entry[i].equals(command))
        {
          return true;
        }
      }
      return false;
    }

    [[nodiscard]] inline auto is_empty() const -> bool
    {
      return count == 0;
    }

    [[nodiscard]] inline auto is_at_top() const -> bool
    {
      return scrolling_count == 0;
    }

    [[nodiscard]] inline auto is_at_bottom() const -> bool
    {
      return scrolling_count == count - 1;
    }

  private:
    String entry[HISTORY_SIZE];
    uint8_t head = 0;
    uint8_t count = 0;
    mutable uint8_t scrolling_count = 0;
    mutable uint8_t scrolling_idx = 0;
};

const String command_history::NO_ENTRY = "";

}  // namespace vsdt

#endif  // VINTAGE_SYSTEMS_DEV__HISTORY_HPP_
