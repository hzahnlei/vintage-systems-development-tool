/*!
 * (c) 2019 Holger Zahnleiter. All righs reserved.
 */

#ifndef VINTAGE_SYSTEMS_DEV__COMMAND_LINE_HPP_
#define VINTAGE_SYSTEMS_DEV__COMMAND_LINE_HPP_

#include "conversion.hpp"

namespace vsdt
{

    /*!
     * /brief A parsable command line (as entered by the user).
     */
    class command_line final
    {
        public:
            [[nodiscard]] auto eof() const -> bool
            {
                return pos >= cmdln.length() || only_whitespace();
            }
            
            [[nodiscard]] auto consume() -> String
            {
                skip_leading_whitespace();
                const auto first_legal_char_pos = pos;
                consume_legal_characters();
                return cmdln.substring(first_legal_char_pos, pos);
            }
            
            auto operator += (const char c) -> command_line&
            {
                cmdln += c;
                return *this;
            }
            
            auto operator = (const String &s) -> command_line&
            {
                cmdln = s;
                pos = 0;
                return *this;
            }
            
            auto operator -- (int) -> command_line&
            {
                if (!cmdln.equals(""))
                {
                    cmdln = cmdln.substring(0, cmdln.length()-1);
                }
                return *this;
            }
            
            explicit inline operator const String &() const
            {
                return cmdln;
            }

        private:
            [[nodiscard]] inline auto only_whitespace() const -> bool
            {
                auto look_ahead_pos = pos;
                while (look_ahead_pos < cmdln.length())
                {
                    if (!is_whitespace(cmdln[look_ahead_pos]))
                    {
                        return false;
                    }
                    look_ahead_pos++;
                }
                return true;
            }

            [[nodiscard]] inline auto skip_leading_whitespace() -> void
            {
                while (pos < cmdln.length() && is_whitespace(cmdln[pos]))
                {
                    pos++;
                }
            }

            [[nodiscard]] inline auto consume_legal_characters() -> void
            {
                while (pos < cmdln.length() && !is_whitespace(cmdln[pos]))
                {
                    pos++;
                }
            }
            
        private:
            String cmdln = "";
            size_t pos = 0;
    };

}  // namespace vsdt

#endif  // VINTAGE_SYSTEMS_DEV__COMMAND_LINE_HPP_
