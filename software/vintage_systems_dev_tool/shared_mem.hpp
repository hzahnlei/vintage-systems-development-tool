/*!
 * (c) 2019 Holger Zahnleiter. All righs reserved.
 */

#ifndef VINTAGE_SYSTEMS_DEV__SHARED_MEM_HPP_
#define VINTAGE_SYSTEMS_DEV__SHARED_MEM_HPP_

#include "low_level_control.hpp"

namespace vsdt
{

    /*!
     * /brief Represents the memory space shared between host and guest CPU.
     */
    class shared_mem final
    {
        public:
            shared_mem( low_level_control &ctl ) : control(ctl)
            {
            }
            
            auto init() -> void
            {
                control.halt_guest();
                control.make_host_the_busmaster();
                control.enable_8k_page_select();
            }
            
            [[nodiscard]] auto peek(const uint16_t addr) const -> uint8_t
            {
                const auto was_guest_running = control.is_guest_running();
                const auto was_guest_the_busmaster = ! control.is_host_the_busmaster();
                control.disable_data_io();
                control.latch_address(addr);
                if (was_guest_running)
                    control.halt_guest();
                control.enable_host_read();
                if (was_guest_the_busmaster)
                    control.make_host_the_busmaster();
                control.enable_data_io();
                const auto mem_value = control.read();
                control.disable_data_io();
                if (was_guest_the_busmaster)
                    control.make_guest_the_busmaster();
                if (was_guest_running)
                    control.make_guest_run();
                return mem_value;
            }
            
            auto poke(const uint16_t addr, const uint8_t val) -> void
            {
                const auto was_guest_running = control.is_guest_running();
                const auto was_guest_the_busmaster = ! control.is_host_the_busmaster();
                control.disable_data_io();
                control.latch_address(addr);
                if (was_guest_running)
                    control.halt_guest();
                control.enable_host_write();
                if (was_guest_the_busmaster)
                    control.make_host_the_busmaster();
                control.enable_data_io();
                control.write(val);
                control.disable_data_io();
                control.enable_host_read();
                if (was_guest_the_busmaster)
                    control.make_guest_the_busmaster();
                if (was_guest_running)
                    control.make_guest_run();
            }

            auto fill(const uint16_t from_addr, const uint16_t to_addr,const uint8_t val) -> void
            {
                const auto was_guest_running = control.is_guest_running();
                const auto was_guest_the_busmaster = !control.is_host_the_busmaster();
                if (was_guest_running)
                    control.halt_guest();
                control.enable_host_write();
                if (was_guest_the_busmaster)
                    control.make_host_the_busmaster();
                for (uint32_t addr = from_addr; addr <= to_addr; addr++)
                {
                    control.disable_data_io();
                    control.latch_address(addr);
                    control.enable_data_io();
                    control.write(val);
                }  
                control.disable_data_io();
                control.enable_host_read();
                if (was_guest_the_busmaster)
                    control.make_guest_the_busmaster();
                if (was_guest_running)
                    control.make_guest_run();
            }
            
        private:
            mutable low_level_control &control;
    };

}  // namespace vsdt

#endif  // VINTAGE_SYSTEMS_DEV__SHARED_MEM_HPP_
