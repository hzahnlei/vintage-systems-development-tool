/*!
 * (c) 2019 Holger Zahnleiter. All righs reserved.
 */

#ifndef VINTAGE_SYSTEMS_DEV__APPLICATION_HPP_
#define VINTAGE_SYSTEMS_DEV__APPLICATION_HPP_

#include "command_line.hpp"
#include "low_level_control.hpp"
#include "shared_mem.hpp"
#include "history.hpp"
#include "conversion.hpp"
#include "intel_hex.hpp"


#define INTRO_ENABLED
#define HELP_ENABLED

namespace vsdt
{
    
    /*!
     * /brief What should I say? The application!
     */
    class application final
    {
            
        public:
            application(low_level_control &ctl) : board(ctl), mem(ctl)
            {
            }
            
            auto init() -> void
            {
                mem.init();
            }
            
            auto print_intro() const -> void
            {
#ifdef INTRO_ENABLED
                give_serial_monitor_a_chance_to_connect();
                Serial.println(F("*******************************************************************************"));
                Serial.println(F("**  Vintage Systems Development Tool, V1.0.0                                 **"));
                Serial.println(F("**  (c) 2019, 2023 by Holger Zahnleiter. All rights reserved.                **"));
                Serial.println(F("*******************************************************************************"));
                Serial.println();
#endif
            }

            auto read_and_execute_command() -> void
            {
                print_prompt();
                read_command_line();
                const auto status = execute_command();
                if (error_code::OK == status)
                {
                    hist.remember((String)cmdln);
                }
                print_status(status);
            }
 
        private:
#ifdef INTRO_ENABLED
            auto give_serial_monitor_a_chance_to_connect() const -> void
            {
                for (auto i = 0U; i < 10; i++)
                {
                    Serial.print('.');
                    delay(500);
                }
                Serial.println();
            }
#endif

            auto print_prompt() const -> void
            {
                Serial.print(F("> "));
            }

            static constexpr char BELL =            0x07;
            static constexpr char BACKSPACE =       0x08;
            static constexpr char NEW_LINE =        '\n';
            static constexpr char CARRIAGE_RETURN = '\r';
            static constexpr char CURSOR_UP =       0x1e;
            static constexpr char CURSOR_DOWN =     0x1f;

            auto read_command_line() -> void
            {
                cmdln = "";
                while (true)
                {
                    while (!Serial.available())
                    {
                        delay(10);
                    }
                    if (Serial.available() > 0)
                    {
                        if (read_single_char())
                        {
                            continue;
                        }
                        break;
                    } 
                }
                Serial.print('\n');
            }

            auto read_single_char() -> bool
            {
                const char c = Serial.read();
                if (NEW_LINE == c)
                {
                    return true;
                }
                else if (CARRIAGE_RETURN == c)
                {
                    return false;
                }
                else if (BACKSPACE == c)
                {
                    delete_last_char();
                }
                else if (CURSOR_UP == c)
                {
                    scroll_back_in_history();
                }
                else if (CURSOR_DOWN == c)
                {
                   scroll_forward_in_history();
                }
                else if (is_printable(c))
                {
                    Serial.print(c); // Echo back the ASCII symbol typed.
                    cmdln += c;
                }
                else
                {
                    // No printable ASCII character.
                    // For this, the terminal must be configured to handle BELL.
                    Serial.print(BELL);
                }
                return true;
            }

            auto delete_last_char() -> void
            {
                if (!((String)cmdln).equals(""))
                {
                    cmdln--;
                    Serial.print(BACKSPACE); // For this, the terminal must be configured to handle BS.
                }
            }

            auto scroll_back_in_history() -> void
            {
                const auto &previous = hist.previous();
                if (command_history::NO_ENTRY == previous)
                {
                    Serial.print(BELL); // For this, the terminal must be configured to handle BELL.
                }
                else
                {
                    for (auto i=0U; i<((String)cmdln).length(); i++)
                    {
                        Serial.print(BACKSPACE); // For this, the terminal must be configured to handle BS.
                    }
                    cmdln = previous;
                    Serial.print(previous);
                }
            }

            auto scroll_forward_in_history() -> void
            {
                const auto &next = hist.next();
                if (command_history::NO_ENTRY == next)
                {
                    Serial.print(BELL); // For this, the terminal must be configured to handle BELL.
                }
                else
                {
                    for (auto i=0U; i<((String)cmdln).length(); i++)
                    {
                        Serial.print(BACKSPACE); // For this, the terminal must be configured to handle BS.
                    }
                    cmdln = next;
                    Serial.print(next);
                }
            }

            enum class error_code : uint8_t
            {
                OK, UNKNOWN_COMMAND, TO_FEW_ARGUMENTS, TO_MANY_ARGUMENTS, NOT_A_NUMBER, NOT_16_BIT, NOT_8_BIT, FAILURE_TO_VERIFY
            };
            
            auto print_status(const error_code status) -> void
            {
                switch (status)
                {
                    case error_code::OK:
                        break;
                    case error_code::UNKNOWN_COMMAND:
                        Serial.println(F("Unknown command. Type ? for help."));
                        break;
                    case error_code::TO_FEW_ARGUMENTS:
                        Serial.println(F("To few arguments."));
                        break;
                    case error_code::TO_MANY_ARGUMENTS:
                        Serial.println(F("To many arguments."));
                        break;
                    case error_code::NOT_A_NUMBER:
                        Serial.println(F("Not a hex number."));
                        break;
                    case error_code::NOT_16_BIT:
                        Serial.println(F("Not 16 bits."));
                        break;
                    case error_code::NOT_8_BIT:
                        Serial.println(F("Not 8 bits."));
                        break;
                    case error_code::FAILURE_TO_VERIFY:
                        Serial.println(F("Verification error."));
                        break;
                    default:
                        Serial.println(F("UNHANDLED STATUS CODE!"));
                }
            }

            [[nodiscard]] auto execute_command() -> error_code
            {
                if (cmdln.eof())
                {
                    return error_code::OK;
                }
                const auto cmd = cmdln.consume();
                if (cmd.equals(F("?")))
                {
                    return execute_help_or_peek();
                }
                else if (cmd.equals(F("!")))
                {
                    return execute_poke();
                }
                else if (cmd.equals(F("*")))
                {
                    return execute_fill();
                }
                else if (cmd.equals(F("=")))
                {
                    return execute_verify();
                }
                else if (cmd.equals(F("reset")))
                {
                    return execute_reset_guest();
                }
                else if (cmd.equals(F("status")))
                {
                    return execute_status();
                }
                else if (cmd.equals(F("halt")))
                {
                    return execute_halt_guest();
                }
                else if (cmd.equals(F("run")))
                {
                    return execute_run_guest();
                }
                else if (cmd.equals(F("int")))
                {
                    return execute_interrupt_guest();
                }
                else if (cmd.startsWith(F(":")))
                {
                    return execute_intel_hex_record();
                }
                else
                {
                    return error_code::UNKNOWN_COMMAND;
                }
            }


            [[nodiscard]] auto execute_intel_hex_record() -> error_code
            {
                const auto record = intel::hex::record_from((String)cmdln);
                if (record.is_data())
                {
                    auto i = 0U;
                    for (uint32_t addr = record.address(); i < record.byte_count(); addr++, i++)
                    {
                        mem.poke((uint16_t)addr, record.data_byte()[i]);
                    }
                    return error_code::OK;
                }
                else if (record.is_end())
                {
                    return error_code::OK;
                }
                else
                {
                    return error_code::UNKNOWN_COMMAND;
                }
            }

            [[nodiscard]] auto execute_help_or_peek() -> error_code
            {
                return cmdln.eof()
                    ? execute_help()
                    : execute_peek();
            }

            [[nodiscard]] auto execute_help() -> error_code
            {
#ifdef HELP_ENABLED
                //                        10        20        30        40        50        60        70        80
                //                .........|.........|.........|.........|.........|.........|.........|.........|
                Serial.println(F("?"));
                Serial.println(F("        Prints this overview of availables commands."));
                Serial.println(F("? <addr>"));
                Serial.println(F("        Prints content of memory location <addr>."));
                Serial.println(F("? <addr1> <addr2>"));
                Serial.println(F("        Prints content of memory from location <addr1> to location <addr2>."));
                Serial.println(F("! <addr> <val>"));
                Serial.println(F("        Sets content of memory location <addr> to <val>."));
                Serial.println(F("! <addr> <val1> <val2>..."));
                Serial.println(F("        Sets content of successive memory locations beginning at <addr> to "));
                Serial.println(F("        <val1>, <val2> etc."));
                Serial.println(F("* <addr1> <addr2> <val>"));
                Serial.println(F("        Fills a successive area of memory from <addr1> to <addr2> with a value"));
                Serial.println(F("        of <val>."));
                Serial.println(F("= <addr1> <addr2>"));
                Serial.println(F("        Verifies that all memory cells from <addr1> to <addr2> are working"));
                Serial.println(F("        correctly. (Memory gets written, read back and compared.)"));
                Serial.println(F("reset"));
                Serial.println(F("        Resets guest CPU and peripherals."));
                Serial.println(F("status"));
                Serial.println(F("        Whether or not the guest CPU is halted."));
                Serial.println(F("halt"));
                Serial.println(F("        Halt guest CPU."));
                Serial.println(F("run"));
                Serial.println(F("        Let guest CPU run."));
                Serial.println(F("int"));
                Serial.println(F("        Interrupts the guest CPU (short pulse, negative edge)."));
                Serial.println(F(":"));
                Serial.println(F("        Intel hex records. Recognizes types 0 and 1."));
                Serial.println();
                Serial.println(F("<addr> - A 16 bit value."));
                Serial.println(F("<val>  - An 8 bit value."));
                Serial.println();
                Serial.println(F("Example values: 1af9, ff (all values are hexadecimal)."));
                Serial.println();
#endif
                return error_code::OK;
            }

            template<class T>
            class argument final
            {
                public:
                    explicit argument(const error_code stat): stat(stat), val(0)
                    {
                    }
                    explicit argument(const T val): stat(error_code::OK), val(val)
                    {
                    }
                    [[nodiscard]] inline auto status() const -> error_code
                    {
                        return stat;
                    }
                    [[nodiscard]] inline auto value() const -> const T
                    {
                        return val;
                    }
                    [[nodiscard]] inline auto has_error() const -> const T
                    {
                        return error_code::OK != stat;
                    }
                private:
                    const error_code stat;
                    const T val;
            };
            
            [[nodiscard]] auto acquire_8bit_argument() -> argument<uint8_t>
            {
                if (cmdln.eof())
                {
                    return argument<uint8_t>{error_code::TO_FEW_ARGUMENTS};
                }
                const auto text = cmdln.consume();
                if (!is_hex_number(text))
                {
                    return argument<uint8_t>{error_code::NOT_A_NUMBER};
                }
                if (!is_8bit_hex_number(text))
                {
                    return argument<uint8_t>{error_code::NOT_8_BIT};
                }
                return argument<uint8_t>{uint8_from(text)};
            }

            [[nodiscard]] auto acquire_16bit_argument() -> argument<uint16_t>
            {
                if (cmdln.eof())
                {
                    return argument<uint16_t>{error_code::TO_FEW_ARGUMENTS};
                }
                const auto text = cmdln.consume();
                if (!is_hex_number(text))
                {
                    return argument<uint16_t>{error_code::NOT_A_NUMBER};
                }
                if (!is_16bit_hex_number(text))
                {
                    return argument<uint16_t>{error_code::NOT_16_BIT};
                }
                return argument<uint16_t>{uint16_from(text)};
            }
            
            [[nodiscard]] auto execute_peek() -> error_code
            {
                const auto addr = acquire_16bit_argument();
                if (addr.has_error())
                {
                    return addr.status();
                }
                return cmdln.eof()
                    ? execute_peek_single_mem_cell(addr.value())
                    : execute_mem_dump(addr.value());
            }

            [[nodiscard]] auto execute_peek_single_mem_cell(const uint16_t addr) -> error_code
            {
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                Serial.print(hex_representation_of(addr));
                Serial.print(F(": "));
                Serial.println(hex_representation_of(mem.peek(addr)));
                return error_code::OK;
            }

            [[nodiscard]] auto execute_mem_dump(const uint16_t from_addr) -> error_code
            {
                const auto to_addr = acquire_16bit_argument();
                if (to_addr.has_error())
                {
                    return to_addr.status();
                }
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                String ascii;
                auto byte_count = 0U;
                for (uint32_t addr = from_addr; addr <= to_addr.value(); addr++)
                {
                    if (0 == byte_count % 16)
                    {
                        if (byte_count > 0)
                        {
                            Serial.print(F("  "));
                            Serial.println(ascii);
                            ascii = "";
                        }
                        Serial.print(hex_representation_of((uint16_t)addr));
                        Serial.print(F(": "));
                    }
                    const auto val = mem.peek(addr);
                    ascii += ascii_representation_of(val);
                    Serial.print(hex_representation_of(val));
                    Serial.print(' ');
                    byte_count++;
                }
                for ( ; byte_count % 16; byte_count++)
                {
                    Serial.print(F("   "));
                }
                Serial.print(F("  "));
                Serial.println(ascii);
                return error_code::OK;
            }
            
            [[nodiscard]] auto execute_poke() -> error_code
            {
                const auto addr = acquire_16bit_argument();
                if (addr.has_error())
                {
                    return addr.status();
                }
                
                uint16_t offset = 0U;
                while (!cmdln.eof())
                {
                    const auto val = acquire_8bit_argument();
                    if (val.has_error())
                    {
                        return val.status();
                    }
                    mem.poke(addr.value() + offset, val.value());
                    offset++;
                }
                return error_code::OK;
            }

            [[nodiscard]] auto execute_fill() -> error_code
            {
                const auto from_addr = acquire_16bit_argument();
                if (from_addr.has_error())
                {
                    return from_addr.status();
                }
                const auto to_addr = acquire_16bit_argument();
                if (to_addr.has_error())
                {
                    return to_addr.status();
                }
                const auto val = acquire_8bit_argument();
                if (val.has_error())
                {
                    return val.status();
                }
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                
                mem.fill(from_addr.value(), to_addr.value(), val.value());
                 
                return error_code::OK;
            }

            [[nodiscard]] auto execute_verify() -> error_code
            {
                const auto from_addr = acquire_16bit_argument();
                if (from_addr.has_error())
                {
                    return from_addr.status();
                }

                const auto to_addr = acquire_16bit_argument();
                if (to_addr.has_error())
                {
                    return to_addr.status();
                }
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                
                auto status = verify(from_addr.value(), to_addr.value(), 0x00);
                if ( error_code::OK != status )
                {
                    return status;
                }
                status = verify(from_addr.value(), to_addr.value(), 0xff);
                if ( error_code::OK != status )
                {
                    return status;
                }
                status = verify(from_addr.value(), to_addr.value());
                if ( error_code::OK != status )
                {
                    return status;
                }
                
                return error_code::OK;
            }

            [[nodiscard]] auto verify(const uint16_t from_addr, const uint16_t to_addr, const uint8_t expected) -> error_code
            {
                for (uint32_t addr = from_addr; addr <= to_addr; addr++)
                {
                    mem.poke(addr, expected);
                }

                for (uint32_t addr = from_addr; addr <= to_addr; addr++)
                {
                    const auto actual = mem.peek(addr);
                    if (expected != actual)
                    {
                        Serial.print(F("Expected ["));
                        Serial.print(hex_representation_of((uint16_t)addr));
                        Serial.print(F("] = "));
                        Serial.print(hex_representation_of(expected));
                        Serial.print(F(" but was "));
                        Serial.print(hex_representation_of(actual));
                        Serial.print(F(". "));
                        return error_code::FAILURE_TO_VERIFY;
                    }
                }
                return error_code::OK;
            }

            /*!
             * Interval of numbers written for verification [0, START_OVER[.
             * It is no coincidence that the number is a prime number.
             * I explicitly wanted boundries not to be powers of two!
             */
            static constexpr uint8_t START_OVER = 251;
            
            [[nodiscard]] auto verify(const uint16_t from_addr, const uint16_t to_addr) -> error_code
            {
                auto i = 0U;
                for (uint32_t addr = from_addr; addr <= to_addr; addr++)
                {
                    mem.poke(addr, (uint8_t)(i % START_OVER));
                    i++;
                }
                i = 0;
                for (uint32_t addr = from_addr; addr <= to_addr; addr++)
                {
                    const auto actual = mem.peek(addr);
                    const auto expected = (uint8_t)(i % START_OVER);
                    if (expected != actual)
                    {
                        Serial.print(F("Expected ["));
                        Serial.print(hex_representation_of((uint16_t)addr));
                        Serial.print(F("] = "));
                        Serial.print(hex_representation_of(expected));
                        Serial.print(F(" but was "));
                        Serial.print(hex_representation_of(actual));
                        Serial.print(F(". "));
                        return error_code::FAILURE_TO_VERIFY;
                    }
                    i++;
                }
                return error_code::OK;
            }

            [[nodiscard]] auto execute_reset_guest() -> error_code
            {
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                board.reset_guest();
                return error_code::OK;
            }

            [[nodiscard]] auto execute_status() -> error_code
            {
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                Serial.print(F("Guest "));
                Serial.println(board.is_guest_running()
                    ? F("running.")
                    : F("halted."));
                Serial.print(board.is_host_the_busmaster()
                    ? F("Host")
                    : F("Guest"));
                Serial.println(F(" is busmaster."));
                return error_code::OK;
            }

            [[nodiscard]] auto execute_halt_guest() -> error_code
            {
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                board.halt_guest();
                board.make_host_the_busmaster();
                return error_code::OK;
            }

            [[nodiscard]] auto execute_run_guest() -> error_code
            {
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                board.make_guest_the_busmaster();
                board.make_guest_run();
                return error_code::OK;
            }

            [[nodiscard]] auto execute_interrupt_guest() -> error_code
            {
                if (!cmdln.eof())
                {
                    return error_code::TO_MANY_ARGUMENTS;
                }
                board.interrupt_pulse();
                return error_code::OK;
            }
            
        private:
            low_level_control& board;
            shared_mem mem;
            command_line cmdln;
            command_history hist;
    };
    
}  // namespace vsdt

#endif  // VINTAGE_SYSTEMS_DEV__APPLICATION_HPP_
