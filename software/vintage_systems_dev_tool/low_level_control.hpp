/*!
 * (c) 2019 Holger Zahnleiter. All righs reserved.
 */

#ifndef VINTAGE_SYSTEMS_DEV__LOW_LEVEL_CONTROL_HPP_
#define VINTAGE_SYSTEMS_DEV__LOW_LEVEL_CONTROL_HPP_

namespace vsdt
{

    constexpr uint8_t ALL_PINS_ACT_AS_OUTPUTS = B11111111;
    constexpr uint8_t ALL_PINS_ACT_AS_INPUTS =  ~ALL_PINS_ACT_AS_OUTPUTS;
    
    #define SIG_DATA_AND_ADDR_OUT_PORT        PORTB
    #define SIG_DATA_AND_ADDR_IN_PORT         PINB
    #define SIG_DATA_AND_ADDR_OUT_PORT_CONFIG DDRB

    enum class control_signal : uint8_t
    {
        RUN_GUEST =        B00000001,
        BUSMASTER_HOST =   B00000010,
        RESET_GUEST =      B00000100,
        R_W_HOST =         B00001000,
        ENABLE_8K_PG_SEL = B00010000
    };
    
    constexpr uint8_t HOST_IS_BUSMASTER_AND_GUEST_IS_HALTED =
        static_cast<uint8_t>(control_signal::BUSMASTER_HOST) |
        static_cast<uint8_t>(control_signal::R_W_HOST);
    
    #define STROBE_PORT PORTF
    #define STROBE_PORT_CONFIG DDRF
    // ATmega32U4: PF0/ADC0, Arduino Micro: A5
    constexpr uint8_t STROBE_CTL_SIGNAL = B00000001;
    // ATmega32U4: PF1/ADC1, Arduino Micro: A4
    constexpr uint8_t STROBE_ADDR_HIGH =  B00000010;
    // ATmega32U4: PF4/ADC4, Arduino Micro: A3
    constexpr uint8_t STROBE_ADDR_LOW =   B00010000;
    // ATmega32U4: PF5/ADC5, Arduino Micro: A2
    constexpr uint8_t DISABLE_IO =        B00100000;
    // ATmega32U4: PF6/ADC6, Arduino Micro: A1
    constexpr uint8_t M68_E =             B01000000;
    // ATmega32U4: PF7/ADC7, Arduino Micro: A0
    constexpr uint8_t RW =                B10000000;

    constexpr uint8_t NO_STROBE_NO_IO_NO_M68E = B00000000 | DISABLE_IO | RW;

    constexpr uint16_t RESET_GUEST_DELAY_MS = 100U;

    /*!
     * /brief Represents the wires on the board.
     * With the help of this class, the host CPU directly controls the signals on
     * the circuit board. For example, the guest CPU can be halted, reset etc.
     * There is no deep logic in here. The code is relatively simple. However,
     * attention must be payed to set and clear all signals in the right order.
     */
    class low_level_control final
    {
        public:
            low_level_control()
            {
                SIG_DATA_AND_ADDR_OUT_PORT_CONFIG = ALL_PINS_ACT_AS_OUTPUTS;
                SIG_DATA_AND_ADDR_OUT_PORT = control_lines;
                STROBE_PORT_CONFIG = ALL_PINS_ACT_AS_OUTPUTS;
                STROBE_PORT = strobe_lines;
                pinMode(7, OUTPUT);
                digitalWrite(7, LOW);
            }

            auto interrupt_pulse() -> void
            {
              digitalWrite(7, HIGH);
//              delay(1);
              digitalWrite(7, LOW);
            }
            
            auto make_guest_run() -> void
            {
                  set_control_line(static_cast<uint8_t>(control_signal::RUN_GUEST));
            }
            auto halt_guest() -> void
            {
                  clear_control_line(static_cast<uint8_t>(control_signal::RUN_GUEST));
            }
            [[nodiscard]] auto is_guest_running() const -> bool
            {
                  return is_control_line_set(static_cast<uint8_t>(control_signal::RUN_GUEST));
            }
    
            auto make_host_the_busmaster() -> void
            {
                  set_control_line(static_cast<uint8_t>(control_signal::BUSMASTER_HOST));
            }
            auto make_guest_the_busmaster() -> void
            {
                  clear_control_line(static_cast<uint8_t>(control_signal::BUSMASTER_HOST));
            }
            [[nodiscard]] auto is_host_the_busmaster() const -> bool
            {
                return is_control_line_set(static_cast<uint8_t>(control_signal::BUSMASTER_HOST));
            }
    
            auto reset_guest() -> void
            {
                set_control_line(static_cast<uint8_t>(control_signal::RESET_GUEST));
                delay(RESET_GUEST_DELAY_MS);
                clear_control_line(static_cast<uint8_t>(control_signal::RESET_GUEST));
            }
            
            auto enable_host_read() -> void
            {
                set_control_line(static_cast<uint8_t>(control_signal::R_W_HOST));
            }
            auto enable_host_write() -> void
            {
                clear_control_line(static_cast<uint8_t>(control_signal::R_W_HOST));
            }
            [[nodiscard]] auto is_host_writing_enabled() const -> bool
            {
                return is_control_line_set(static_cast<uint8_t>(control_signal::R_W_HOST));
                return 0 == (strobe_lines & RW);
            }

            auto signal_read_rd() -> void
            {
                strobe_lines |= RW;
                STROBE_PORT = strobe_lines;
            }

            auto signal_write_wr() -> void
            {
                strobe_lines &= ~RW;
                STROBE_PORT = strobe_lines;
            }
            
            auto latch_address( const uint16_t address ) -> void
            {
                SIG_DATA_AND_ADDR_OUT_PORT_CONFIG = ALL_PINS_ACT_AS_OUTPUTS;
                latch_addr_low(address);
                latch_addr_high(address);
            }

            /*!
             * Assumes address has been latched, host is busmaster and R/W set to read.
             */
            inline auto read() -> uint8_t
            {
                SIG_DATA_AND_ADDR_OUT_PORT_CONFIG = ALL_PINS_ACT_AS_INPUTS;
                SIG_DATA_AND_ADDR_OUT_PORT = 0x00;
                signal_read_rd();
                delay(0);
                //m68_enable_pulse(); // Same as 6502 Phi2
                m68_enable();
                const uint8_t mem_value = SIG_DATA_AND_ADDR_IN_PORT;
                m68_disable();

                SIG_DATA_AND_ADDR_OUT_PORT_CONFIG = ALL_PINS_ACT_AS_OUTPUTS;
                return mem_value;
            }
            
            /*!
             * Assumes address has been latched, host is busmaster and R/W set to write.
             */
            inline auto write(const uint8_t val) -> void
            {
                SIG_DATA_AND_ADDR_OUT_PORT_CONFIG = ALL_PINS_ACT_AS_OUTPUTS;
                SIG_DATA_AND_ADDR_OUT_PORT = val;
                delay(0);
                signal_write_wr();
                delay(0);
                //delay(0);
                m68_enable_pulse(); // Same as 6502 Phi2
                signal_read_rd();
            }

            auto enable_8k_page_select() -> void
            {
                  set_control_line(static_cast<uint8_t>(control_signal::ENABLE_8K_PG_SEL));
            }
            auto disable_8k_page_select() -> void
            {
                  clear_control_line(static_cast<uint8_t>(control_signal::ENABLE_8K_PG_SEL));
            }
            [[nodiscard]] auto is_8k_page_select_enabled() const -> bool
            {
                return is_control_line_set(static_cast<uint8_t>(control_signal::ENABLE_8K_PG_SEL));
            }

            auto enable_data_io() -> void
            {
                strobe_lines &= ~DISABLE_IO;
                STROBE_PORT = strobe_lines;
            }
            auto disable_data_io() -> void
            {
                strobe_lines |= DISABLE_IO;
                STROBE_PORT = strobe_lines;
            }

        private:
            auto set_control_line(const uint8_t line) -> void
            {
                control_lines |= line;
                SIG_DATA_AND_ADDR_OUT_PORT = control_lines;
                strobe_lines |= STROBE_CTL_SIGNAL;
                STROBE_PORT = strobe_lines;
                strobe_lines &= ~STROBE_CTL_SIGNAL;
                STROBE_PORT = strobe_lines;
            }
            auto clear_control_line(const uint8_t line) -> void
            {
                control_lines &= ~line;
                SIG_DATA_AND_ADDR_OUT_PORT = control_lines;
                strobe_lines |= STROBE_CTL_SIGNAL;
                STROBE_PORT = strobe_lines;
                strobe_lines &= ~STROBE_CTL_SIGNAL;
                STROBE_PORT = strobe_lines;
            }
            [[nodiscard]] auto is_control_line_set(const uint8_t line) const -> bool
            {
                return 0U != (control_lines & line);
            }

            auto latch_addr_low(const uint16_t address) -> void
            {
                SIG_DATA_AND_ADDR_OUT_PORT = (uint8_t)address;
                strobe_lines |= STROBE_ADDR_LOW;
                STROBE_PORT = strobe_lines;
                strobe_lines &= ~STROBE_ADDR_LOW;
                STROBE_PORT = strobe_lines;
            }
            auto latch_addr_high(const uint16_t address) -> void
            {
                SIG_DATA_AND_ADDR_OUT_PORT = (uint8_t)(address >> 8);
                strobe_lines |= STROBE_ADDR_HIGH;
                STROBE_PORT = strobe_lines;
                strobe_lines &= ~STROBE_ADDR_HIGH;
                STROBE_PORT = strobe_lines;
            }

            auto m68_enable() -> void
            {
                strobe_lines |= M68_E;
                STROBE_PORT = strobe_lines;
                delay(0); // Give the vintage chips some time...
                //delay(0);
                //delay(0);
            }
            auto m68_disable() -> void
            {
                strobe_lines &= ~M68_E;
                STROBE_PORT = strobe_lines;
                delay(0); // Give the vintage chips some time...
                //delay(0);
                //delay(0);
            }
            auto m68_enable_pulse() -> void
            {
                m68_enable();
                m68_disable();
            }
            
        private:
            uint8_t control_lines = HOST_IS_BUSMASTER_AND_GUEST_IS_HALTED;
            uint8_t strobe_lines = NO_STROBE_NO_IO_NO_M68E;
    };

}  // namespace vsdt

#endif  // VINTAGE_SYSTEMS_DEV__LOW_LEVEL_CONTROL_HPP_
