/*!
 * (c) 2019 Holger Zahnleiter. All righs reserved.
 */

#ifndef VINTAGE_SYSTEMS_DEV__INTEL_HEX_HPP_
#define VINTAGE_SYSTEMS_DEV__INTEL_HEX_HPP_

#include "conversion.hpp"

namespace vsdt
{
namespace intel
{
namespace hex
{

    enum class record_type_t : uint8_t
    {
        DATA = 0x00, END = 0x01, UNKNOWN
    };
    
    constexpr uint8_t MAX_NB_BYTES = 16;
    
    class record final
    {
        public:
            static const record END;
            static const record UNKNOWN;
            record(const uint16_t addr = 0U) : rec_type(record_type_t::DATA), addr(addr)
            {
            }
        private:
            record(const record_type_t rec_type, const uint16_t addr = 0U) : rec_type(rec_type), addr(addr)
            {
            }
        public:
            ~record() = default;
            [[nodiscard]] auto is_data() const -> bool
            {
                return rec_type == record_type_t::DATA;
            }
            [[nodiscard]] auto is_end() const -> bool
            {
                return rec_type == record_type_t::END;
            }
            [[nodiscard]] auto address() const -> uint16_t
            {
                return addr;
            }
            [[nodiscard]] auto data_byte() const -> uint8_t const * const
            {
                return data;
            }
            [[nodiscard]] auto byte_count() const -> uint8_t
            {
                return count;
            }
            [[nodiscard]] auto add(const uint8_t data_byte) -> bool
            {
                if (count < MAX_NB_BYTES)
                {
                    data[count] = data_byte;
                    count++;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        private:
            const record_type_t rec_type;
            const uint16_t addr = 0;
            uint8_t data[MAX_NB_BYTES];
            uint8_t count = 0;
    };

    const record record::END{record_type_t::END};
    const record record::UNKNOWN{record_type_t::UNKNOWN};

    [[nodiscard]] auto record_from(const String& textual_representation) -> record
    {
        if (textual_representation.length() >= 10)
        {
            const auto byte_count_text = textual_representation.substring(1, 3);
            if (is_8bit_hex_number(byte_count_text))
            {
                const auto byte_count = uint8_from(byte_count_text);
                const auto address_text = textual_representation.substring(3, 7);
                if (is_16bit_hex_number(address_text))
                {
                    const auto address = uint16_from(address_text);
                    const auto record_type_text = textual_representation.substring(7, 9);
                    if (is_8bit_hex_number(record_type_text))
                    {
                        const auto record_type = uint8_from(record_type_text);
                        if (record_type == (uint8_t)record_type_t::END)
                        {
                            return record::END;
                        }
                        else if (record_type == (uint8_t)record_type_t::DATA && textual_representation.length() == 9 + 2*byte_count + 2)
                        {
                            record data_record{address};
                            for (auto i=0U; i<=byte_count; i++)
                            {
                                const auto pos = 9 + 2 * i; 
                                const auto byte_text = textual_representation.substring(pos, pos+2);
                                if (is_8bit_hex_number(byte_text))
                                {
                                    const auto byte_val = uint8_from(byte_text);
                                    data_record.add(byte_val);
                                }
                                else
                                {
//Serial.println("Not a data byte");
                                    return record::UNKNOWN;
                                }
                            }
                            return data_record;
                        }
//Serial.println("Unknown record type");
                    }
//Serial.println("Not a record type");
                }
//Serial.println("Not a address word");
            }
//Serial.println("Not a count");
        }
//Serial.print("To few bytes. ");
//Serial.print("Expected >=10 but was ");
//Serial.println(textual_representation.length());
        return record::UNKNOWN;
    }

}  //namespace hex
}  //namespace intel
}  //namespace vsdt

#endif  //VINTAGE_SYSTEMS_DEV__INTEL_HEX_HPP_
