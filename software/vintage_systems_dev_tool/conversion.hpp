/*!
 * (c) 2019 Holger Zahnleiter. All righs reserved.
 */

#ifndef VINTAGE_SYSTEMS_DEV__CONVERSION_HPP_
#define VINTAGE_SYSTEMS_DEV__CONVERSION_HPP_

namespace vsdt
{
    
    [[nodiscard]] inline auto is_hex_digit(const char c) -> bool
    {
        return (c >= 'a' && c <= 'f') || (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F');
    }

    [[nodiscart]] auto is_hex_number(const String& val) -> bool
    {
        for (auto i=0U; i<val.length(); i++)
        {
            if (!is_hex_digit(val[i]))
            {
                return false;
            }
        }
        return true;
    }
    
    [[nodiscart]] auto is_16bit_hex_number(const String& val) -> bool
    {
        return is_hex_number(val) && val.length() >= 1 && val.length() <= 4;
    }
    
    [[nodiscart]] auto is_8bit_hex_number(const String& val) -> bool
    {
        return is_hex_number(val) && val.length() >= 1 && val.length() <= 2;
    }

    /*!
     * Assumes val to be a correct lexical representation of an 8 bit value.
     */
    [[nodiscard]] auto uint8_from(const String& val) -> uint8_t
    {
        return (uint8_t)strtol(val.c_str(), 0, 16);
    }
    
    /*!
     * Assumes val to be a correct lexical representation of a 16 bit value.
     */
    [[nodiscard]] auto uint16_from(const String& val) -> uint16_t
    {
        return (uint16_t)strtol(val.c_str(), 0, 16);
    }

    [[nodiscard]] inline auto is_printable(const uint8_t val) -> bool
    {
        return val > 31 && val < 127;
    }
    
    [[nodiscard]] auto ascii_representation_of(const uint8_t val) -> char
    {
        return is_printable(val) ? (char)val : '.';
    }
    
    static constexpr char* const LUT = "0123456789ABCDEF";
    
    [[nodiscard]] auto hex_representation_of(const uint8_t val) -> String
    {
        String result;
        result += LUT[val >> 4];
        result += LUT[val & 15];
        return result;
    }
    
    [[nodiscard]] auto hex_representation_of(const uint16_t val) -> String
    {
        String result;
        result += LUT[val >> 12];
        result += LUT[(val >> 8) & 15];
        result += LUT[(val >> 4) & 15];
        result += LUT[val & 15];
        return result;
    }

    [[nodiscard]] inline auto is_whitespace(const char c) -> bool
    {
        return c <= 32 || c >= 127;
    }

}  // namespace vsdt

#endif  // VINTAGE_SYSTEMS_DEV__CONVERSION_HPP_
