/*!
 * (c) 2019 Holger Zahnleiter. All righs reserved.
 */

// The following features consume quite some memory.
// Enable them on demand but be aware that the ammount of data that can be pasted into the console is reduced thereby.
//#define INTRO_ENABLED
//#define HELP_ENABLED

#include "low_level_control.hpp"
#include "application.hpp"

vsdt::low_level_control board;
vsdt::application app{board};

auto setup() -> void
{
    Serial.begin(9600);
    delay(10);
    app.init();
    app.print_intro();
}

auto loop() -> void
{    
    app.read_and_execute_command();
}
